SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `Order_Details`
--

-- --------------------------------------------------------

--
-- Table structure for table `Order_info`
--

CREATE TABLE `Order_info` (
  `id` int UNSIGNED NOT NULL,
  `order_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Order_info`
--

INSERT INTO `Order_info` (`id`, `order_id`) VALUES
(1, 789),
(2, 879),
(3, 978);

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int UNSIGNED NOT NULL,
  `prod_Id` int UNSIGNED NOT NULL,
  `order_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `prod_Id`, `order_id`) VALUES
(1, 3, 879),
(2, 3, 978),
(3, 1, 978);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int UNSIGNED NOT NULL,
  `product_name` varchar(20) NOT NULL,
  `product_code` int UNSIGNED NOT NULL,
  `product_desc` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_code`, `product_desc`) VALUES
(1, 'product1', 132, 'This is product1'),
(2, 'product2', 213, 'This is product2'),
(3, 'product3', 123, 'This is product1'),
(4, 'product4', 231, 'This is product4'),
(5, 'product5', 312, 'This is product5'),
(6, 'product6', 321, 'This is product1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Order_info`
--
ALTER TABLE `Order_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`),
  ADD KEY `id` (`id`),
  ADD KEY `order_id_2` (`order_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `prod_Id` (`prod_Id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `product_code` (`product_code`),
  ADD KEY `product_code_2` (`product_code`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_code_3` (`product_code`,`product_desc`);
ALTER TABLE `product` ADD FULLTEXT KEY `product_desc` (`product_desc`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Order_info`
--
ALTER TABLE `Order_info`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `Order_info` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_item_ibfk_2` FOREIGN KEY (`prod_Id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
