SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `User_Details`
--

-- --------------------------------------------------------

--
-- Table structure for table `User_info`
--

CREATE TABLE `User_info` (
  `id` int NOT NULL,
  `name` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `User_info`
--

INSERT INTO `User_info` (`id`, `name`, `password`, `create_date`, `status`) VALUES
(1, 'Abhisek', 'abhisek', '2021-03-01 13:55:00', 1),
(2, 'Kaustuv', 'kaustuv', '2021-03-04 14:02:44', 0),
(3, 'Gautam', 'gautam', '2021-03-05 13:56:57', 0),
(4, 'Udeshya', 'udeshya', '2021-03-02 13:57:53', 0),
(5, 'Anurag', 'anurag', '2021-03-26 13:58:27', 1),
(6, 'Chandan', 'chandu', '2021-03-17 13:58:50', 0),
(7, 'Sanket', 'samantaray', '2021-03-22 13:59:16', 1),
(8, 'Sajit', 'sajit', '2021-03-23 13:59:42', 1),
(9, 'Omshuvam', 'omu', '2021-03-15 14:00:21', 1),
(10, 'Amit', 'amit', '2021-03-20 14:04:57', 0),
(16, 'Suvankar', 'suvam', '2021-03-18 14:02:12', 0);


--
-- Table structure for table `deleted_info`
--

CREATE TABLE `deleted_info` (
  `user_id` int NOT NULL,
  `name` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `Creation_time` datetime NOT NULL,
  `Delete_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `deleted_info`
--

INSERT INTO `deleted_info` (`user_id`, `name`, `password`, `status`, `Creation_time`, `Delete_time`) VALUES
(15, 'Adesh', 'adesh', 0, '2021-03-10 14:01:19', '2021-03-30 14:36:16'),
(17, 'Debashish', 'debashish', 1, '2021-03-13 14:03:26', '2021-03-30 14:36:32');

-- --------------------------------------------------------

--
-- Table structure for table `Password_changed`
--

CREATE TABLE `Password_changed` (
  `user_id` int NOT NULL,
  `old_password` varchar(20) NOT NULL,
  `new_password` varchar(20) NOT NULL,
  `pass_change_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Password_changed`
--

INSERT INTO `Password_changed` (`user_id`, `old_password`, `new_password`, `pass_change_date`) VALUES
(7, 'sankettt', 'samantaray', '2021-03-30 14:49:49');

-- --------------------------------------------------------
--
-- Triggers `User_info`
--
DELIMITER $$
CREATE TRIGGER `delete_Info` BEFORE DELETE ON `User_info` FOR EACH ROW INSERT INTO deleted_info VALUES(OLD.id, OLD.name, OLD.password, OLD.status,OLD.create_date, NOW())
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `password_change` AFTER UPDATE ON `User_info` FOR EACH ROW INSERT INTO Password_changed VALUES(OLD.id, OLD.password, NEW.password, NOW())
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `User_info`
--
ALTER TABLE `User_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `User_info`
--
ALTER TABLE `User_info`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;
